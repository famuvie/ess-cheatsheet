Contributing to Emacs + ESS cheatsheet

Contributions are very welcome.
Either for correcting mistakes, or for suggesting changes/additions.

Please feel free to file an [issue](https://gitlab.com/famuvie/ess-cheatsheet/issues).

However, this is a *personal* cheatsheet and I don't intend to cope with everyone's needs.
Feel free to fork this cheatsheet and curtomize it to your needs.