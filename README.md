Emacs + ESS cheatsheet

A very basic cheatsheet to start using Emacs with R.

Contrary to other (overwhelming) cheatsheets I have found, I tried to organize it around commonly desired (for me) actions in a typical workflow.
